# plumled

plumled - PLantUMl live web EDitor.
PlantUML Web Live editor test application.

```
PlantUML is an open-source tool allowing users to create UML diagrams from a plain text language. 

The language of PlantUML is an example of a Domain-specific language.

It uses Graphviz software to lay out its diagrams.
```

https://plantuml.com/ru/


[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/volodink/plumled) 
[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/volodink/plumled)

